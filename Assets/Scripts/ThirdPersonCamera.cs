using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ThirdPersonCamera : MonoBehaviour
{
	[SerializeField] private Transform target;
	[SerializeField] private Camera camera;
	public Camera Camera {  get { return camera; } }

	[SerializeField] private CinemachineVirtualCamera cameraSettings;
	private Cinemachine3rdPersonFollow follow;


	[SerializeField] private float cameraFollowSpeed = 0.2f;
	[SerializeField] private float cameraLookSpeed = 2;
	[SerializeField] private float cameraPivotSpeed = 2;

	[SerializeField] private Vector2 pivotAngleLimits;
	[SerializeField] private Vector2 zoomLimits;
	private float lookAngle;
	private float pivotAngle;

	public float scrollSpeed;

	private Vector2 mouseInput;

	private void Start()
	{
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
		follow = cameraSettings.GetCinemachineComponent<Cinemachine3rdPersonFollow>();
	}

	public void SetTarget(Transform t)
	{
		target = t;
		cameraSettings.Follow = t;
	}

	public void RotateCamera()
	{
		lookAngle = lookAngle + (mouseInput.x * cameraLookSpeed);
		pivotAngle = pivotAngle - (mouseInput.y * cameraPivotSpeed);

		pivotAngle = Mathf.Clamp(pivotAngle, pivotAngleLimits.x, pivotAngleLimits.y);

		float finalZoom = follow.CameraDistance - Input.mouseScrollDelta.y;
		follow.CameraDistance = finalZoom;
		follow.CameraDistance = Mathf.Clamp(follow.CameraDistance, zoomLimits.x, zoomLimits.y);

		Vector3 rot = Vector3.zero;
		rot.y = lookAngle;
		rot.x = pivotAngle;
		Quaternion targetRot = Quaternion.Euler(rot);
		target.transform.rotation = targetRot;
	
	}

	// Update is called once per frame
	void LateUpdate()
    {
		if (target == null)
			return;
		mouseInput = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
		RotateCamera();
    }
}

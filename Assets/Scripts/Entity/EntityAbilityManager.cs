using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Entity))]
public class EntityAbilityManager : MonoBehaviour
{
	public AbilityData selectedAbilitySO;

	private Ability selectedAbility;

	private Ability abilityInUse;
	private float abilityTimer;
	private float tickTimer;

	int castStateIndex = 0;

	public enum AbilityActions
	{
		CastBegin,
		CastTick,
		CastEnd,
		ChannelBegin,
		ChannelTick,
		ChannelEnd,
		RecoverBegin,
		RecoverTick,
		RecoverEnd
	}

	public enum CombatStates
	{
		none,
		casting,
		channeling,
		recovering
	}

	public CombatStates currentCombatState;

	private void Start()
	{
		selectedAbility = new Ability_Melee(selectedAbilitySO);
		selectedAbility.SetEntity(GetComponent<Entity>());
	}

	public void PrimaryInput()
	{
		if (currentCombatState == CombatStates.none)
		{
			UseAbility(selectedAbility, CombatStates.casting);
		}
	}

	private void SwitchCombatState(CombatStates state)
	{
		currentCombatState = state;
		switch (state)
		{
			case CombatStates.none:
				break;
			case CombatStates.casting:
				if (abilityInUse != null)
					abilityInUse.DoAction(AbilityActions.CastBegin);
				break;
			case CombatStates.channeling:
				if (abilityInUse != null)
					abilityInUse.DoAction(AbilityActions.ChannelBegin);
				break;
			case CombatStates.recovering:
				if (abilityInUse != null)
					abilityInUse.DoAction(AbilityActions.RecoverBegin);
				break;
		}
		UIBarManager.instance.SetCastBarName(currentCombatState.ToString());

	}
	private void UseAbility(Ability a, CombatStates stateToStart)
	{
		abilityInUse = a;
		SwitchCombatState(stateToStart);
	}

	public void Update()
	{
		UpdateCurrentAbility();
	}
	private void UpdateCurrentAbility()
	{
		if (abilityInUse != null)
		{
			bool complete = false;
			if (currentCombatState != CombatStates.none)
			{
				abilityTimer += Time.deltaTime;
				tickTimer += Time.deltaTime;
			}

			AbilityActions tickAction = AbilityActions.CastTick;
			AbilityActions endAction = AbilityActions.CastEnd;

			switch (currentCombatState)
			{
				case CombatStates.none:
					break;
				case CombatStates.casting:
					tickAction = AbilityActions.CastTick;
					endAction = AbilityActions.CastEnd;
					break;
				case CombatStates.channeling:
					tickAction = AbilityActions.ChannelTick;
					endAction = AbilityActions.ChannelEnd;
					break;
				case CombatStates.recovering:
					tickAction = AbilityActions.RecoverTick;
					endAction = AbilityActions.RecoverEnd;
					break;
			}

			UIBarManager.instance.DisplayAndUpdateCastBar(abilityInUse.Data.stateSequence[castStateIndex].sequenceTime, abilityTimer);
			if (tickTimer >= abilityInUse.Data.stateSequence[castStateIndex].sequenceTickTime)
			{
				abilityInUse.DoAction(tickAction);
				tickTimer = 0;
			}
			if (abilityTimer > abilityInUse.Data.stateSequence[castStateIndex].sequenceTime)
			{
				abilityInUse.DoAction(endAction);
				complete = true;
			}

			if (complete)
			{
				castStateIndex++;
				abilityTimer = 0;
				UIBarManager.instance.HideCastBar();
				if (castStateIndex >= abilityInUse.Data.stateSequence.Length)
				{
					abilityInUse = null;
					castStateIndex = 0;
					SwitchCombatState(CombatStates.none);
				}
				else
				{
					SwitchCombatState(abilityInUse.Data.stateSequence[castStateIndex].combatState);
				}
			}
		}
	}
	
}
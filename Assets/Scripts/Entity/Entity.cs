using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyResources;
using CMF;
public class Entity : MonoBehaviour, IDamageable
{
	[SerializeField] private EntityData entityData;
	public EntityData EntityData { get { return entityData; } }

	[SerializeField] private Animator animator;
	public Animator Animator { get { return animator; } }

	[SerializeField] private EntityAbilityManager entityAbilityManager;
	public EntityAbilityManager EntityAbilityManager { get { return entityAbilityManager; } }

	[SerializeField] private CharacterInput entityInput;
	public CharacterInput EntityInput { get { return entityInput; } }

	public int DamageableID => this.GetInstanceID();

	[SerializeField] private Transform healthBarFollow;

	public Transform cameraFollowTarget;

	private void Awake()
	{
		Initialise();
	}
	private void Start()
	{
	
	
		UIBarManager.instance.CreateHealthBar(this, healthBarFollow);
		EventManager.RegisterEvent(EventIDs.RecieveDamage, DamageRecieved);
	}

	private void Initialise()
	{
		if (entityAbilityManager == null)
			entityAbilityManager = GetComponent<EntityAbilityManager>();
		if (entityInput == null)
			entityInput = GetComponent<CharacterInput>();

		entityData.Init();
	}

	public void DamageRecieved(object _source, object[] _args)
	{
		int entityID = (int)_args[0];
		float damage = (float)_args[1];

		if (entityID != DamageableID)
			return;

		entityData.Health.AdjustValue(-damage);
		if(entityData.Health.IsEmpty)
		{
			OnDie();
		}
	}

	public void OnDie()
	{
		entityData.Health.ClearBars();
		Destroy(this.gameObject);		
	}
}


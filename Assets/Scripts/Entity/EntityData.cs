using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyResources;

[System.Serializable]
public class EntityData 
{
	[SerializeField] private Resource health;
	public Resource Health { get { return health; } }

	[SerializeField] private string entityName;
	public string Name { get { return entityName; } }


	public void Init()
	{
		health = new Resource(health);
		entityName = "Character name";
	}

	private void LoadData(EntityData data)
	{
		health = data.health;
		entityName = data.entityName;
		Init();
	}
}

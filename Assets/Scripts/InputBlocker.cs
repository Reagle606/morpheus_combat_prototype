using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputBlocker 
{
    private List<string> blockers = new List<string>();
    public bool IsBlocked { get { return blockers.Count == 0 ? false : true; } }

    public void SetBlocker(bool value, string ID)
	{
		if (value && !blockers.Contains(ID))
		{
			blockers.Add(ID);
		}
		else if (!value && blockers.Contains(ID))
		{
			blockers.Remove(ID);
		}
	}

}

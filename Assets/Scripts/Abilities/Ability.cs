using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability 
{
	protected Entity caster;
	protected AbilityData data;
	public AbilityData Data { get { return data; } }

	public Ability(AbilityData _data)
	{
		SetAbilityData(_data);
	}

	public virtual void SetAbilityData(AbilityData _data)
	{
		data = _data;
	}

	public void SetEntity(Entity e)
	{
		caster = e;
	}

	public void DoAction(EntityAbilityManager.AbilityActions action)
	{
		switch (action)
		{
			case EntityAbilityManager.AbilityActions.CastBegin:
				CastBegin();
				break;
			case EntityAbilityManager.AbilityActions.CastTick:
				CastTick();
				break;
			case EntityAbilityManager.AbilityActions.CastEnd:
				CastEnd();
				break;
			case EntityAbilityManager.AbilityActions.ChannelBegin:
				ChannelBegin();
				break;
			case EntityAbilityManager.AbilityActions.ChannelTick:
				ChannelTick();
				break;
			case EntityAbilityManager.AbilityActions.ChannelEnd:
				ChannelEnd();
				break;
			case EntityAbilityManager.AbilityActions.RecoverBegin:
				RecoverBegin();
				break;
			case EntityAbilityManager.AbilityActions.RecoverTick:
				RecoverTick();
				break;
			case EntityAbilityManager.AbilityActions.RecoverEnd:
				RecoverEnd();
				break;
		}
	}

	public virtual void CastBegin()
	{

	}
	public virtual void CastTick()
	{

	}
	public virtual void CastEnd()
	{

	}

	public virtual void ChannelBegin()
	{

	}
	public virtual void ChannelTick()
	{

	}
	public virtual void ChannelEnd()
	{

	}

	public virtual void RecoverBegin()
	{

	}
	public virtual void RecoverTick()
	{

	}
	public virtual void RecoverEnd()
	{

	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AbilityData : ScriptableObject
{
	[SerializeField]
	public AbilitySequence[] stateSequence;
	
	
}

[System.Serializable]
public class AbilitySequence
{
	public EntityAbilityManager.CombatStates combatState;
	public float sequenceTime;
	public float sequenceTickTime;
}



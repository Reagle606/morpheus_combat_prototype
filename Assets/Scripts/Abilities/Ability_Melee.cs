using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_Melee : Ability
{
	public Ability_Melee(AbilityData _data) : base (_data)
	{

	}

	public override void CastBegin()
	{
		caster.Animator.SetTrigger("Attack");
	}

	public override void CastTick()
	{

	}

	public override void CastEnd()
	{

	}

	public override void ChannelBegin()
	{
		Collider[] cols = Physics.OverlapSphere(caster.transform.position, 2);
		for (int i = 0; i < cols.Length; i++)
		{
			IDamageable d = cols[i].GetComponent<IDamageable>();
			if (d != null && d != caster.GetComponent<IDamageable>())
			{
				EventManager.FireEvent(this, EventIDs.RecieveDamage, d.DamageableID, 25.0f);
			}
		}
	}

	public override void ChannelTick()
	{

	}

	public override void ChannelEnd()
	{

	}

	public override void RecoverBegin()
	{

	}

	public override void RecoverTick()
	{

	}

	public override void RecoverEnd()
	{

	}
}

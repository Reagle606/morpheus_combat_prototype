using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable
{
	int DamageableID { get; }
	public void DamageRecieved(object _source, object[] _args);
	public void OnDie();
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBarManager : MonoBehaviour
{
	public static UIBarManager instance;

	public UIBar healthBarPrefab;
	public UIBar castBar;

	public Canvas canvas;

	private List<UIBar> healthbars;

	private void Awake()
	{
		if (instance == null)
			instance = this;

		healthbars = new List<UIBar>();
	}

	public void CreateHealthBar(Entity entity, Transform follow)
	{
		UIBar bar = Instantiate(healthBarPrefab, canvas.transform);
		entity.EntityData.Health.AddBar(bar);
		bar.followPoint = follow;
		bar.worldSpaceFollow = true;
		bar.billboard = true;
		healthbars.Add(bar);
	}

	public void DisplayAndUpdateCastBar(float max, float current)
	{
		if (!castBar.gameObject.activeSelf)
			castBar.gameObject.SetActive(true);

		castBar.UpdateBar(current, max);
	
	}

	public void SetCastBarName(string name)
	{
		castBar.amountText.text = name;
	}
	public void HideCastBar()
	{
		if (castBar.gameObject.activeSelf)
			castBar.gameObject.SetActive(false);
	}

	public void RemoveBar(UIBar bar)
	{
		if(healthbars.Contains(bar))
		{
			healthbars.Remove(bar);
		}
	}

	private void Update()
	{
		for(int i = 0; i < healthbars.Count; i++)
		{
			healthbars[i].ExternalUpdate();
		}
		castBar.ExternalUpdate();
	}
}

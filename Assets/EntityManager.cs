using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityManager : MonoBehaviour
{
	public static EntityManager Instance;

	public void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}

	public ThirdPersonCamera characterCamera;
	public Entity defaultEntityPrefab;

	public void Start()
	{
		SpawnPlayableCharacter(defaultEntityPrefab, Vector3.zero, Quaternion.identity);
		SpawnEntity(defaultEntityPrefab, new Vector3(1, 0, 0), Quaternion.identity);
	}

	public static Entity SpawnEntity(Entity prefab, Vector3 position, Quaternion rotation, bool useInput = false)
	{
		Entity e = Instantiate(prefab, position, rotation);
		return e;
	}

	public void SpawnPlayableCharacter(Entity prefab, Vector3 position, Quaternion rotation)
	{
		Entity e = SpawnEntity(prefab, position, rotation, true);
		e.EntityInput.SetEnabled(true);
		characterCamera.SetTarget(e.cameraFollowTarget);
		e.GetComponent<CMF.AdvancedWalkerController>().cameraTransform = characterCamera.Camera.transform;
	}
}
